from flask import Flask
from flask import jsonify

from tortoise import run_async
from tables import *

run_async(run_orm())
app = Flask(__name__)


@app.route('/start')
async def index():

    return "cool"


@app.route('/getTable')
async def async_get_urls_v1():

    req = await Request.all()
  
