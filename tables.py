from tortoise.models import Model
from tortoise import fields
import asyncio
from tortoise import Tortoise


class Request(Model):
    id = fields.BigIntField(pk=True)
    RequestNumber = fields.TextField(default="")
    RequestType = fields.TextField(default="")
    Address = fields.TextField(default="")
    Description = fields.TextField(default="")
    CompletionDescription = fields.TextField(default="")
    Status = fields.TextField(default="")
    RequestPriority = fields.TextField(default="")
    Operator = fields.TextField(default="")
    ExecutingOrganization= fields.TextField(default="")
    CreateDateTime = fields.TextField(default="")
    LastModificationDateTime = fields.TextField(default="")
    SubmittedToOrgDate = fields.TextField(default="")
    CompletedDateTime = fields.TextField(default="")


async def run_orm():
    await Tortoise.init(config={
        "connections": {
            "default": {

                "engine": "tortoise.backends.asyncpg",
                "credentials": {
                    "host": "bli3lab.tech",
                    "port": "32783",
                    "database": None,
                    "user": "root",
                    "password": 'lab_2035'
                }
            }
        },
        "apps": {
            "models": {
                "models": ["tables"],

            }
        },
    })
    await Tortoise.generate_schemas()

if __name__ == '__main__':
    asyncio.run(run_orm())
